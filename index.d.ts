export interface BinTesterOptions {
  /**
   * An array of command line arguments and values to use when executing the
   * package bin command.
   *
   * @default []
   */
  binArguments?: string[];

  /**
   * The bin command to be executed.
   */
  command?: string;

  /**
   * Environment variable key-value pairs (in addition to process.env).
   *
   * @default {}
   */
  environment?: { [key: string]: string };

  /**
   * The directory with the package bin to be executed.
   *
   * @default './'
   */
  packageDirectory?: string;

  /**
   * A timeout duration in ms.
   *
   * @default 0
   */
  timeout?: number;

  /**
   * The working directory to execute the package bin command.
   *
   * @default './'
   */
  workingDirectory?: string;
}

export interface BinTesterResults {
  /**
   * The exit code from executing the bin command.
   */
  code: number;

  /**
   * The error thrown during execution, if any.
   */
  error: Error;

  /**
   * The stderr output from executing the bin command.
   */
  stderr: string;

  /**
   * The stdout output from executing the bin command.
   */
  stdout: string;
}

/**
 * Execute bin as specified in package.json, with the specified arguments,
 * returning a promise that always resolves if the command was executed,
 * even if and Error was thrown during execution.  A promise rejection
 * indicates and error executing the command.
 *
 * @param   {BinTesterOptions}          [options] Bin command options.
 * @returns {Promise<BinTesterResults>}           Promise resolving with a
 *                                                BinTesterResults object.
 * @static
 * @public
 */
export function bin({
  binArguments,
  command,
  environment,
  packageDirectory,
  timeout,
  workingDirectory
}?: BinTesterOptions): Promise<BinTesterResults>;
