'use strict';

const recommendedConfig = require('@aarongoldenthal/eslint-config-standard/recommended');

module.exports = [
    ...recommendedConfig,
    {
        files: ['**/*.js'],
        name: 'FIX ME',
        rules: {
            'promise/no-multiple-resolved': 'off'
        }
    },
    {
        ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
        name: 'ignores'
    }
];
