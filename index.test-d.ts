import { expectError, expectType } from 'tsd';
import { bin, BinTesterOptions, BinTesterResults } from './index';

const optionCases: BinTesterOptions[] = [
  { binArguments: ['foo'] },
  { command: 'baz' },
  { environment: { foo: 'bar' } },
  { environment: { foo: 'bar', baz: 'quux' } },
  { packageDirectory: './bar' },
  { timeout: 1000 },
  { workingDirectory: './foo' },
  {
    command: 'baz',
    binArguments: ['foo'],
    environment: { foo: 'bar' },
    packageDirectory: './bar',
    timeout: 1000,
    workingDirectory: './foo'
  }
];

// Test invalid arguments
expectError(bin(false));
expectError(bin({ binArguments: false }));
expectError(bin({ environment: { foo: 'bar', baz: 123 } }));

// Test no arguments
bin();

// Test valid arguments and return types
for (const optionCase of optionCases) {
  expectType<BinTesterResults>(await bin(optionCase));
}
