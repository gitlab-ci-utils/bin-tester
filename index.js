'use strict';

/**
 * Executes the package bin command in the specific folder with the specified
 * arguments and returns the results.
 *
 * @module bin-tester
 */

const { execFile } = require('node:child_process');
const { getBinPath } = require('./lib/package-utils');

const validateBinArguments = (binArguments) => {
    if (binArguments !== undefined && !Array.isArray(binArguments)) {
        throw new TypeError('binArguments must be an array');
    }
};

/**
 * Execute bin as specified in package.json, with the specified arguments,
 * returning a promise that always resolves if the command was executed,
 * even if and Error was thrown during execution.  A promise rejection
 * indicates and error executing the command.
 *
 * @param   {object}          [options]                  Bin command options.
 * @param   {string[]}        [options.binArguments]     An array of command line arguments and
 *                                                       values to use when executing the package
 *                                                       bin command.
 * @param   {string}          [options.workingDirectory] The working directory to execute the package
 *                                                       bin command.
 * @param   {string}          [options.packageDirectory] The directory with the package bin to be executed.
 * @param   {object}          [options.environment]      Environment variable key-value pairs (in addition
 *                                                       to process.env).
 * @param   {string}          [options.command]          The bin command to be executed.
 * @param   {number}          [options.timeout]          A timeout duration in ms.
 * @returns {Promise<object>}                            Promise resolving with an object with the exit
 *                                                       code, error, stdout, and stderr from executing
 *                                                       the package bin.
 * @static
 * @public
 */
// eslint-disable-next-line max-lines-per-function, complexity -- exceeded by formatting, allow low threshold
const bin = ({
    binArguments,
    command = '',
    environment = {},
    packageDirectory = './',
    timeout = 0,
    workingDirectory = './'
} = {}) =>
    // eslint-disable-next-line promise/avoid-new -- promisifying execFile
    new Promise((resolve) => {
        validateBinArguments(binArguments);

        const args = [getBinPath(packageDirectory, command)];
        if (binArguments && binArguments.length > 0) {
            args.push(...binArguments);
        }

        execFile(
            'node',
            args,
            {
                cwd: workingDirectory,
                // The default env value is process.env, so need to add additional values.
                // Without this, PATH is not passed and the "node" command is not found.
                env: { ...process.env, ...environment },
                timeout
            },
            // eslint-disable-next-line promise/prefer-await-to-callbacks -- execFile API
            (error, stdout, stderr) =>
                resolve({
                    code: error && error.code ? error.code : 0,
                    error,
                    stderr,
                    stdout
                })
        );
    });

module.exports.bin = bin;
