'use strict';

/**
 * Contains utilities for retrieving data from package.json files.
 *
 * @module packageUtils
 */

const fs = require('node:fs');
const path = require('node:path');

const fileName = 'package.json';

const getPackageJsonPath = (packageDirectory) =>
    // Application requires package directory to be provided.
    // nosemgrep: path-join-resolve-traversal
    path.resolve(packageDirectory, fileName);

const getPackageJson = (packageJsonPath) =>
    // Application requires package directory to be provided.
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    JSON.parse(fs.readFileSync(packageJsonPath));

/**
 * Returns the absolute path of the `bin` command from the package.json in the specified
 * directory (or current directory if none specified).
 *
 * @param   {string}    packageDirectory The package directory (containing the package.json).
 * @param   {string}    [command]        The name of the bin command to be executed (required if object).
 * @returns {string}                     The absolute path of the `bin` command from the
 *                                       package.json in the specified directory.
 * @throws  {TypeError}                  The package.json does not contain a valid bin property
 *                                       (is undefined, or defined and not a string or object).
 * @static
 * @public
 */
// eslint-disable-next-line complexity -- simpler to read
const getBinPath = (packageDirectory, command) => {
    const packageJsonPath = getPackageJsonPath(packageDirectory);
    const pkg = getPackageJson(packageJsonPath);

    const binType = typeof pkg.bin;
    switch (binType) {
        case 'string': {
            if (!command || command === pkg.name) {
                // Application requires package directory to be provided.
                // nosemgrep: path-join-resolve-traversal
                return path.resolve(packageDirectory, pkg.bin);
            }
            throw new Error(
                `'${command}' is not a valid bin command name in ${packageJsonPath}, ` +
                    `valid names are: ${pkg.name}`
            );
        }
        case 'object': {
            if (command && Object.keys(pkg.bin).includes(command)) {
                // Can only read command from package.json bin object in
                // user specified directory.
                // nosemgrep: eslint.detect-object-injection, path-join-resolve-traversal
                return path.resolve(packageDirectory, pkg.bin[command]);
            }
            throw new Error(
                `'${command}' is not a valid bin command name in ${packageJsonPath}, ` +
                    `valid names are: ${Object.keys(pkg.bin).join(', ')}`
            );
        }
        default: {
            throw new TypeError(
                `${packageJsonPath} does not contain a valid 'bin' property`
            );
        }
    }
};

module.exports.getBinPath = getBinPath;
