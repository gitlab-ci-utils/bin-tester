### Security Issue

<!--
Summarize the security issue, including any relevant links to vulnerability details.

Once submitted the issue will be confidential and only visible to the author and project members. Once the issue is assessed, and resolved if applicable, confidentiality will be removed and the issue will be publicly visible.

If a vulnerability is identified, it will be formally documented as a [CVE](https://about.gitlab.com/security/cve/).
-->

/label ~Security
/confidential
