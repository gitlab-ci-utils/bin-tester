#!/usr/bin/env node
'use strict';

const repeatInterval = 1000;

const print = () => {
    console.log('This is a bin printing to console endlessly');
    setTimeout(print, repeatInterval);
};

print();
