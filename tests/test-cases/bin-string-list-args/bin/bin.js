#!/usr/bin/env node
'use strict';

// Exclude first 2 args which are the "node" executable and "bin" values
// eslint-disable-next-line no-magic-numbers -- index of first arg
console.log(`args: ${process.argv.splice(2).join(', ')}`);
