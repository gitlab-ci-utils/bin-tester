'use strict';

const path = require('node:path');
const { bin } = require('../index');

describe('bin-tester', () => {
    const testNoPackage = './tests/test-cases';
    const testPackageBinInvalid = './tests/test-cases/bin-invalid';
    const testPackageNoBin = './tests/test-cases/no-bin';
    const testPackageObjectMultiple = './tests/test-cases/bin-object-multiple';
    const testPackageObjectSingle = './tests/test-cases/bin-object-single';
    const testPackageStringListArguments =
        './tests/test-cases/bin-string-list-args';
    const testPackageStringCwd = './tests/test-cases/bin-string-cwd';
    const testPackageStringEnvironment = './tests/test-cases/bin-string-env';
    const testPackageStringConsoleOutput = './tests/test-cases/bin-string/';
    const testPackageStringError = './tests/test-cases/bin-string-error';
    const testPackageNeverEnding = './tests/test-cases/bin-never-ending';

    const invalidBinPropertyError = "'bin' property";
    const invalidCommandError = 'not a valid bin command';

    it('should throw for package.json with no bin', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageNoBin };
        await expect(() => bin(options)).rejects.toThrow(
            invalidBinPropertyError
        );
    });

    it('should throw for package.json with invalid bin', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageBinInvalid };
        await expect(() => bin(options)).rejects.toThrow(
            invalidBinPropertyError
        );
    });

    it('should throw if no package.json is found in the package directory', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testNoPackage };
        await expect(() => bin(options)).rejects.toThrow('ENOENT');
    });

    it('should throw if provided binArguments that is not an array', async () => {
        expect.assertions(1);
        const options = {
            binArguments: 0,
            packageDirectory: testPackageStringConsoleOutput
        };
        await expect(() => bin(options)).rejects.toThrow('binArguments');
    });

    it('should throw if the bin property of package.json is an object and a command is not provided', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageObjectSingle };
        await expect(() => bin(options)).rejects.toThrow(invalidCommandError);
    });

    it('should throw if the bin property of package.json is a string and a command is not package name', async () => {
        expect.assertions(1);
        const options = {
            command: 'foo',
            packageDirectory: testPackageStringConsoleOutput
        };
        await expect(() => bin(options)).rejects.toThrow(invalidCommandError);
    });

    it('should throw if the command cannot be found in the specified package.json bin property', async () => {
        expect.assertions(1);
        const options = {
            command: 'foo',
            packageDirectory: testPackageObjectSingle
        };
        await expect(() => bin(options)).rejects.toThrow(invalidCommandError);
    });

    it('should pass no arguments to bin if no binArguments provided', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageStringListArguments };
        // Test case outputs arguments to stdout
        const expectedResults = { stdout: 'args: \n' };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should pass arguments to bin if binArguments provided', async () => {
        expect.assertions(1);
        const binArguments = ['-c', 'my-config'];
        const options = {
            binArguments,
            packageDirectory: testPackageStringListArguments
        };
        // Test case outputs arguments to stdout
        const expectedResults = {
            stdout: `args: ${binArguments.join(', ')}\n`
        };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should execute in the current directory if no workingDirectory provided', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageStringCwd };
        // Test case outputs workingDirectory to stdout
        const expectedResults = { stdout: `${process.cwd()}\n` };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should execute in the specified directory if workingDirectory provided', async () => {
        expect.assertions(1);
        const workingDirectory = './tests/test-cases';
        const options = {
            packageDirectory: testPackageStringCwd,
            workingDirectory
        };
        // Test case outputs workingDirectory to stdout
        const expectedResults = {
            stdout: `${path.resolve(process.cwd(), workingDirectory)}\n`
        };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should pass additional environment variables to bin if environment provided', async () => {
        expect.assertions(1);
        const environment = { FOO: 'bar' };
        const options = {
            environment,
            packageDirectory: testPackageStringEnvironment
        };
        // Test case outputs environment variable FOO to stdout
        const expectedResults = { stdout: `env: FOO=${environment.FOO}\n` };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should not pass additional environment variables to bin if no environment provided', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageStringEnvironment };
        // Test case outputs environment variable FOO to stdout
        const expectedResults = { stdout: 'env: FOO=\n' };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should look in the current directory for package.json if no packageDirectory provided', async () => {
        expect.assertions(1);
        // This package has no bin, so if run in current directory then throws a 'bin' property error
        await expect(() => bin()).rejects.toThrow(invalidBinPropertyError);
    });

    it('should look in the specified directory for package.json if packageDirectory provided', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageStringConsoleOutput };
        const expectedResults = {
            code: 0,
            stdout: 'This is a bin with a string\n'
        };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should return stdout and stderr from the executed bin command', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageStringConsoleOutput };
        // Test case outputs to stdout and stderr
        const expectedResults = {
            stderr: 'This is an error from a bin with a string\n',
            stdout: 'This is a bin with a string\n'
        };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should return error code 0 if command executed successfully', async () => {
        expect.assertions(1);
        const options = { packageDirectory: testPackageStringConsoleOutput };
        // eslint-disable-next-line unicorn/no-null -- consistent behavior with execFile
        const expectedResults = { code: 0, error: null };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should return non-zero error code and error if command did not execute successfully', async () => {
        expect.assertions(2);
        const options = { packageDirectory: testPackageStringError };
        // Test case throws an error
        const expectedResults = { code: 1, error: 'This is a test failure' };
        const results = await bin(options);
        expect(results.code).toStrictEqual(expectedResults.code);
        expect(results.error.message).toMatch(expectedResults.error);
    });

    it('should successfully execute command for bin object with a single entry', async () => {
        expect.assertions(1);
        const options = {
            command: 'single',
            packageDirectory: testPackageObjectSingle
        };
        const expectedResults = {
            code: 0,
            stdout: 'This is a bin object with a single command\n'
        };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should successfully execute command for bin object with multiple entries', async () => {
        expect.assertions(1);
        const command = 'command1';
        const options = {
            command,
            packageDirectory: testPackageObjectMultiple
        };
        const expectedResults = {
            code: 0,
            stdout: `This is ${command} from a bin object with multiple commands\n`
        };
        const results = await bin(options);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should stop executing after some time if a timeout is provided', async () => {
        expect.assertions(4);
        const options = {
            command: 'never-ending',
            packageDirectory: testPackageNeverEnding,
            timeout: 2500
        };
        const message = 'This is a bin printing to console endlessly\n';
        const expectedResults = {
            code: 0,
            error: 'Command failed', // The process timing out generates an error
            stderr: '',
            stdout: `${message}${message}${message}`
        };
        const results = await bin(options);

        expect(results.stdout).toStrictEqual(expectedResults.stdout);
        expect(results.code).toStrictEqual(expectedResults.code);
        expect(results.stderr).toStrictEqual(expectedResults.stderr);
        expect(results.error.message).toMatch(expectedResults.error);
    });
});
