'use strict';

const path = require('node:path');

const { getBinPath } = require('../lib/package-utils');

describe('getBinPath', () => {
    const testProjectObjectSingle = './tests/test-cases/bin-object-single/';
    const testProjectString = './tests/test-cases/bin-string/';

    const invalidCommandError = 'not a valid bin command name';

    it('should throw if not passed a valid package directory string', () => {
        expect.assertions(1);
        expect(() => getBinPath()).toThrow(
            /^.*path.+argument.+type.+string.*$/
        );
    });

    it('should throw with file not found if specified location does not contain a package.json', () => {
        expect.assertions(1);
        const packageDirectory = './tests/test-cases/';
        expect(() => getBinPath(packageDirectory)).toThrow('ENOENT');
    });

    it('should throw with invalid bin if package.json does not contain a bin property', () => {
        expect.assertions(1);
        const packageDirectory = './tests/test-cases/no-bin/';
        expect(() => getBinPath(packageDirectory)).toThrow(
            "does not contain a valid 'bin' property"
        );
    });

    it('should throw if package.json bin property is not a string or object', () => {
        expect.assertions(1);
        const packageDirectory = './tests/test-cases/bin-invalid/';
        expect(() => getBinPath(packageDirectory)).toThrow(
            "does not contain a valid 'bin' property"
        );
    });

    it('should throw if bin is an object and command is not provided', () => {
        expect.assertions(1);
        expect(() => getBinPath(testProjectObjectSingle)).toThrow(
            invalidCommandError
        );
    });

    it('should throw if specified command is not found in package.json bin property', () => {
        expect.assertions(1);
        const command = 'foo';
        expect(() => getBinPath(testProjectObjectSingle, command)).toThrow(
            invalidCommandError
        );
    });

    it('should throw if command is not package name and package.json bin is a string', () => {
        expect.assertions(1);
        const command = 'foo';
        expect(() => getBinPath(testProjectString, command)).toThrow(
            invalidCommandError
        );
    });

    it('should return absolute path of bin file if package.json in package directory contains a bin object with a single property', () => {
        expect.assertions(1);
        const binString = './bin/bin3.js';
        const command = 'single';
        const binPath = getBinPath(testProjectObjectSingle, command);
        expect(binPath).toStrictEqual(
            path.resolve(process.cwd(), testProjectObjectSingle, binString)
        );
    });

    it('should return absolute path of bin file if package.json in package directory contains a bin object with a multiple properties', () => {
        expect.assertions(1);
        const binString = './bin/bin2.js';
        const packageDirectory = './tests/test-cases/bin-object-multiple/';
        const command = 'command2';
        const binPath = getBinPath(packageDirectory, command);
        expect(binPath).toStrictEqual(
            path.resolve(process.cwd(), packageDirectory, binString)
        );
    });

    it('should return absolute path of bin file if package.json in package directory contains a bin string and command not provided', () => {
        expect.assertions(1);
        const binString = './bin/bin.js';
        const packageDirectory = testProjectString;
        const binPath = getBinPath(packageDirectory);
        expect(binPath).toStrictEqual(
            path.resolve(process.cwd(), packageDirectory, binString)
        );
    });

    it('should return absolute path of bin file if package.json in package directory contains a bin string and valid command provided', () => {
        expect.assertions(1);
        const binString = './bin/bin.js';
        const packageDirectory = testProjectString;
        const command = 'bin-string';
        const binPath = getBinPath(packageDirectory, command);
        expect(binPath).toStrictEqual(
            path.resolve(process.cwd(), packageDirectory, binString)
        );
    });
});
