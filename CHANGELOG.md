# Changelog

## v6.0.0 (2024-04-297)

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#36)

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#35)

## v5.0.0 (2023-12-17)

### Changed

- BREAKING: The `bin` command now fails if `package.bin` is a string and
  an invalid `command` is specified (i.e. `command` is specified and does not
  match the package `name`). Previously, the `command` was ignored unless
  `package.bin` was an object. (#29)
- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11) and added
  support for Node 21 (released 2023-10-17). Compatible with all current and
  LTS releases (^18.12.0 || >=20.0.0). (#32, #33)
- BREAKING: Updated package `main` to `exports`, so only the default export is
  available.

### Fixed

- Updated invalid `command` errors to include all valid `commmand` values.

### Miscellaneous

- Updated package to publish to NPM with
  [provenance](https://github.blog/2023-04-19-introducing-npm-package-provenance/).

## v4.0.1 (2023-06-03)

### Fixed

- Fixed TypeScript type declarations comments. (#30)

## v4.0.0 (2023-05-28)

Additional details on the changes in this release can be found in
[this blog post](https://aarongoldenthal.com/posts/bin-tester-v4.0.0-released/).

### Changed

- BREAKING: Moved from default export to named export `bin` and updated the
  function to accept a single `options` object argument instead of individual
  arguments. See the README for complete details. (#25)
- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#27)
- BREAKING: Added TypeScript type declarations. (#28)

### Fixed

- Refactored logic to get `bin` command from package.json for readability.
  (#21)

## v3.0.1 (2023-02-05)

### Fixed

- Refactored code for changes in latest `eslint` rules.

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#22)

## v3.0.0 (2022-06-16)

### Changed

- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#18, #19)

### Fixed

- Updated to latest dependencies, including resolving CVE-2022-0536 and CVE-2021-44906 (both dev only)

### Miscellaneous

- Refactored code to comply with latest `eslint` config
- Added `prettier` and disabled `eslint` formatting rules. (#17)
- Added test coverage threshold requirements (#20)
- Updated project policy documents (CODE_OF_CONDUCT.md, CONTRIBUTING.md, SECURITY.md) (#16)

## v2.0.1 (2021-08-29)

### Fixed

- Refactored external process setup to use object spread for combining settings (#14)
- Updated to latest dependencies

### Miscellaneous

- Configured [renovate](https://docs.renovatebot.com/) for dependency updates (#15)

## v2.0.0 (2021-06-02)

### Changed

- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#12)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.3.2 (2021-05-10)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities
- Add missing test case for invalid `bin` (#11)

### Miscellaneous

- Optimize published package to only include the minimum required files (#10)

## v1.3.1 (2021-03-14)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated pipeline to use standard NPM package collection (#9)

## v1.3.0 (2021-02-09)

### Added

- Added option to specify a timeout after which the bin command will terminate and the promise will resolve (thanks @clementoriol for the MR)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.2.0 (2020-11-28)

### Added

- Added support for package.json bin properties that are objects (in addition to strings) (#5)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#7) and GitLab Releaser (#8)

## v1.1.1 (2020-11-20)

### Fixed

- Fixed documentation errors for environment variables

## v1.1.0 (2020-11-20)

### Added

- Added support for providing environment variables (#6)

### Fixed

- Updated to latest dependencies

## v1.0.0 (2020-10-24)

### Added

- Added option to specify the package location (#1)

### Changed

- Renamed arguments for clarity and consistency

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated testing for more complete coverage (#4)

## v0.5.0 (2020-09-20)

Initial implementation
